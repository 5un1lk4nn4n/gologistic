from django.contrib.auth.models import User
from rest_framework import authentication, permissions
from rest_framework.response import Response
from rest_framework.views import APIView


class ListOrders(APIView):
    """
    View to list orders.
    It is available to public
    """

    def get(self, request, format=None):
        """
        Return a list of all orders.
        """
        return Response([])
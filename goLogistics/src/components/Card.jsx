import { Card, Statistic } from 'antd';

const CardBox = ({ title, value, precision, color, icon, suffix }) => (
  <Card bordered={false}>
    <Statistic
      title={title}
      value={value}
      precision={precision}
      valueStyle={{
        color: color,
      }}
      prefix={icon}
      suffix={suffix}
    />
  </Card>
);

export default CardBox;

import { AntDesignOutlined } from '@ant-design/icons';
import { Avatar, Typography } from 'antd';
const TopBar = () => (
  <div
    style={{
      padding: 5,
      display: 'flex',
      alignItems: 'center',
      gap: 10,
    }}
  >
    <Avatar size={50} icon={<AntDesignOutlined />} />
    <Typography.Title level="4">Go Logistic!</Typography.Title>
  </div>
);

export default TopBar;

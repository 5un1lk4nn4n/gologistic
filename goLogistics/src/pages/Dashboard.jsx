import cubejs from '@cubejs-client/core';
import { QueryBuilder, QueryRenderer } from '@cubejs-client/react';
import { Divider, Select, Skeleton } from 'antd';
import moment from 'moment';
// eslint-disable-next-line no-unused-vars
import React, { useEffect, useState } from 'react';
import {
  Bar,
  BarChart,
  ResponsiveContainer,
  Tooltip,
  XAxis,
  YAxis,
} from 'recharts';
import CardBox from '../components/Card.jsx';
import TopBar from '../components/TopBar.jsx';

// eslint-disable-next-line no-undef
const cubejsApi = cubejs(import.meta.env.VITE_REACT_APP_CUBEJS_TOKEN, {
  // eslint-disable-next-line no-undef
  apiUrl: import.meta.env.VITE_REACT_APP_API_URL,
});

const dateFormatter = (item) => moment(item).format('MMM YY');
const defaultValueQuery = {
  measures: ['Users.count'],
  dimensions: ['Users.city'],
  timeDimensions: [
    {
      dimension: 'Users.signedUp',
      granularity: 'month',
      dateRange: ['2016-01-31', '2020-12-31'],
    },
  ],
};
const Dashboard = () => {
  const [rangeType, setRangeType] = useState('month');
  const [query, setQuery] = useState(defaultValueQuery);

  useEffect(() => {
    // update granuality whenever the range is changed
    applyNewQuery(query);
  }, [rangeType]);

  const applyNewQuery = (query) => {
    let newQuery = query;
    if (newQuery.timeDimensions && newQuery.timeDimensions[0]) {
      newQuery = {
        ...newQuery,
        timeDimensions: [
          {
            ...newQuery.timeDimensions[0],
            granularity: rangeType,
          },
        ],
      };
    }
    setQuery(newQuery);
  };

  return (
    <div
      style={{
        background: '#edece7',
        minHeight: '100vh',
        minWidth: '100%',
      }}
    >
      <TopBar />
      <Divider plain>Dashboard</Divider>
      <div
        style={{
          padding: 5,
          display: 'flex',
          alignItems: 'center',
          gap: 10,
          justifyContent: 'center',
        }}
      >
        <QueryRenderer
          query={{
            measures: ['Users.count'],
          }}
          cubejsApi={cubejsApi}
          render={({ resultSet }) => {
            if (!resultSet) {
              return <Skeleton />;
            }

            console.log({
              resultSet,
              data: resultSet.tablePivot(),
            });

            const totalUsers = resultSet.tablePivot();

            return (
              <CardBox
                title={'Users'}
                value={totalUsers.length ? totalUsers[0]['Users.count'] : '-'}
              />
            );
          }}
        />
        <QueryRenderer
          query={{
            measures: ['OrderDetails.count'],
          }}
          cubejsApi={cubejsApi}
          render={({ resultSet }) => {
            if (!resultSet) {
              return <Skeleton />;
            }
            const totalOders = resultSet.tablePivot();
            return (
              <CardBox
                title={'Orders'}
                value={totalOders[0]['OrderDetails.count']}
              />
            );
          }}
        />
      </div>
      <QueryBuilder
        query={query}
        setQuery={applyNewQuery}
        cubejsApi={cubejsApi}
        render={({ resultSet }) => {
          if (!resultSet) {
            return <Skeleton />;
          }

          const data = resultSet.chartPivot();
          var totalAreas = [];
          if (data.length > 0) {
            totalAreas = Object.keys(data[0]).filter(
              (col) => !['x', 'xValues'].includes(col)
            );
          }

          return (
            <div
              style={{
                minWidth: 500,
                minHeight: 300,
              }}
            >
              <Select
                defaultValue={'month'}
                style={{ width: 200, marginLeft: 40, marginBottom: 40 }}
                value={rangeType}
                onChange={(value) => {
                  console.log({
                    value,
                  });
                  setRangeType(value);
                }}
                options={[
                  { value: 'month', label: 'Month' },
                  { value: 'year', label: 'Year' },
                ]}
              />
              <ResponsiveContainer
                width={'100%'}
                height={300}
                style={{ minWidth: 400 }}
              >
                <BarChart data={resultSet.chartPivot()}>
                  <XAxis dataKey="x" tickFormatter={dateFormatter} />
                  <YAxis />
                  <Tooltip labelFormatter={dateFormatter} />
                  {totalAreas.map((area) => (
                    <Bar
                      key={area}
                      dataKey={area}
                      fill={
                        '#' +
                        (((1 << 24) * Math.random()) | 0)
                          .toString(16)
                          .padStart(6, '0')
                      }
                    />
                  ))}
                </BarChart>
              </ResponsiveContainer>
            </div>
          );
        }}
      />
    </div>
  );
};

export default Dashboard;

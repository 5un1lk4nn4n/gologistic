cube(`OrderDetails`, {
  sql: `SELECT * FROM public.orders`,

  measures: {
    count: {
      type: `count`,
      drillMembers: [id, createdAt],
    },

    number: {
      sql: `number`,
      type: `sum`,
    },
  },

  dimensions: {
    status: {
      sql: `status`,
      type: `string`,
    },

    id: {
      shown: true,
      sql: `id`,
      type: `number`,
      primaryKey: true,
    },

    createdAt: {
      sql: `created_at`,
      type: `time`,
    },

    completedAt: {
      sql: `completed_at`,
      type: `time`,
    },

    size: {
      sql: `${LineItems.count}`,
      subQuery: true,
      type: 'number',
    },

    price: {
      sql: `${LineItems.price}`,
      subQuery: true,
      type: 'number',
    },
  },
});

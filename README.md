# Go Logistic Dashboard

Dashboard for analytics developed in ReactJS, Cube and Django Rest API

Screenshots:

![image info](Screenshot1.png)

![image info](Screenshot2.png)

## Installation

### Frontend

Install with npm

```bash
  cd goLogistics
  npm install
  npm run dev
```

### Django

Django uses pyenv and poetry for developement environment. Please see [pyenv](https://github.com/pyenv/pyenv) and [poetry](https://python-poetry.org/docs/) official installation before proceeding further.

```bash
  cd data-api
  poetry install
  poetry run python manage.py runserver
```

### Cube

Cube use docker and docker compose. Please [see](https://docs.docker.com/engine/install/) here for installation.

Change Environment as per needed.

```bash
  cd dashboard-api
  docker compose up

```
